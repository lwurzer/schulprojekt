package ch.higgs_01.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import ch.higgs_01.commands.CMD_Admin;
import ch.higgs_01.commands.CMD_Game;
import ch.higgs_01.events.Event_BlockBreak;
import ch.higgs_01.events.Event_Command;
import ch.higgs_01.events.Event_Death;
import ch.higgs_01.events.Event_ItemDrop;
import ch.higgs_01.events.Event_Join;
import ch.higgs_01.events.Event_Leave;
import ch.higgs_01.events.Event_Move;
import ch.higgs_01.events.Event_ProjectileHit;
import ch.higgs_01.game.Game;
import ch.higgs_01.utils.loadConfig;

/**
 * Dieses Plugin wurde von Leo Wurzer erstellt. Es wurde f�r Minecraft mithilfe
 * der Spigot1.7.10.jar (www.spigotmc.org) programmiert
 * 
 * @author Leo Wurzer
 */

public class Main extends JavaPlugin {
	/*
	 * Wird beim Start aufgerufen
	 */
	public void onEnable() {
		if (loadCommands()) {
			logInfo("�e" + "Befehle erfolgreich geladen");
		}
		if (loadEvents()) {
			logInfo("�e" + "Events erfolgreich geladen");
		}
		if (loadUtils()) {
			logInfo("�e" + "Utils erfolgreich geladen");
		}
		logInfo("<-----------LeoCraft---------->");
		logInfo("LeoCraft version " + getDescription().getVersion()
				+ " erfolgreich geladen");
		logInfo("Author: " + getDescription().getAuthors());
		logInfo("");
		logInfo("Danke, dass du mein Plugin gewaehlt hast <3");
		logInfo("<-----------LeoCraft---------->");

	}

	/**
	 * Registriert alle Utils
	 * 
	 * @return ob es erfolgreich ausgef�hrt wurde.
	 */
	private boolean loadUtils() {

		loadConfig.createConfig();
		loadConfig.loadPluginConfig();
		new Game(this);
		return true;
	}

	/**
	 * Registriert alle Events
	 * 
	 * @return ob es erfolgreich ausgef�hrt wurde.
	 */
	private boolean loadEvents() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new Event_Death(), this);
		pm.registerEvents(new Event_Join(this), this);
		pm.registerEvents(new Event_Move(), this);
		pm.registerEvents(new Event_BlockBreak(), this);
		pm.registerEvents(new Event_Command(), this);
		pm.registerEvents(new Event_Leave(), this);
		pm.registerEvents(new Event_ItemDrop(), this);
		pm.registerEvents(new Event_ProjectileHit(), this);
		return true;
	}

	/**
	 * Registriert alle Commands
	 * 
	 * @return ob es erfolgreuch ausgefihrt wurde.
	 */
	private boolean loadCommands() {

		getCommand("lc").setExecutor(new CMD_Game());
		getCommand("lcadmin").setExecutor(new CMD_Admin());
		return true;

	}

	/**
	 * Wird beim Stoppen aufgerufen
	 */
	public void onDisable() {
		cancleSheds();

	}

	/**
	 * Beendet alle Schedulers
	 */
	private void cancleSheds() {
		Bukkit.getScheduler().cancelAllTasks();

	}

	/**
	 * 
	 * @param text
	 *            Text, der ausgegeben werden soll
	 */
	public static void logInfo(String text) {

		Bukkit.getLogger().info(text);
	}

}
