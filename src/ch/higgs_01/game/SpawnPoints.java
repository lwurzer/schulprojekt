package ch.higgs_01.game;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import ch.higgs_01.main.Main;

public class SpawnPoints {

	static String pfad = "plugins/LeoCraft/Locations";
	static File file = new File(pfad, "spawns.yml");
	static FileConfiguration locCfg = YamlConfiguration.loadConfiguration(file);

	/**
	 * Ueberprueft, ob der Ordner existiert
	 */
	static void checkOrdner() {

		File check = new File(pfad);
		if (!check.isDirectory()) {
			check.mkdirs();
		}

	}

	/**
	 * erstellt die Datei
	 */
	public static void createLocations() {

		checkOrdner();
		if (file.exists()) {
			return;
		}

		try {
			locCfg.save(file);
			Main.logInfo("Die Locations datei wurde erstellt.");
		} catch (IOException e) {

		}

	}

	/**
	 * Setzt den Lobby Punkt - dort werden die Spieler hinteleportiert, wenn das
	 * Spiel beendet wurde
	 * 
	 * @param p
	 *            Der Spieler, der den Punkt Setzt
	 */
	public static void setLobbyPoint(Player p) {

		Location loc = p.getLocation();
		/*
		 * Setzt die Werte
		 */
		locCfg.set("lobby.world", loc.getWorld().getName());
		locCfg.set("lobby.x", loc.getX());
		locCfg.set("lobby.y", loc.getY());
		locCfg.set("lobby.z", loc.getZ());
		locCfg.set("lobby.yaw", loc.getYaw());
		locCfg.set("lobby.pitch", loc.getPitch());

		saveLocCfg();

	}

	/**
	 * 
	 * @return Die Location der Lobby
	 */
	public static Location getLobbyLoc() {

		Location loc = new Location(Bukkit.getWorld("world"), 1, 1, 1);

		loc.setWorld(Bukkit.getWorld(locCfg.getString("lobby.world")));
		loc.setX(locCfg.getDouble("lobby.x"));
		loc.setY(locCfg.getDouble("lobby.y"));
		loc.setZ(locCfg.getDouble("lobby.z"));
		loc.setYaw((float) locCfg.getDouble("lobby.yaw"));
		loc.setPitch((float) locCfg.getDouble("lobby.pitch"));

		return loc;

	}

	/**
	 * Setzt einen Spawnpunkt
	 * 
	 * @param p
	 *            Der Spieler, der den Punkt setzt
	 * @param number
	 *            Die Nummer des Spawnpunktes. Ist ein String, weil man als zahl
	 *            auch "next" eingeben kann
	 * @return Gibt die Nummer des Spawnpunktes zurueck
	 */
	public static int setSpawnPoint(Player p, String number) {

		Location loc = p.getLocation();
		/*
		 * Laedt die anzahl Spawnpunkte
		 */
		ConfigurationSection sect = locCfg.getConfigurationSection("spawns");
		StringBuilder spawns = new StringBuilder();
		int lenght = 0;
		// Public Warps
		try {
			for (String home1 : sect.getKeys(false)) {
				if (!spawns.toString().isEmpty())
					spawns.append(", ");
				spawns.append(home1);
				lenght++;
			}

		} catch (NullPointerException e) {
			lenght = 0;
		}
		int next = lenght + 1;
		String nextString = "" + next;

		if (number.equalsIgnoreCase("next")) {

			locCfg.set("spawns." + nextString + ".world", loc.getWorld()
					.getName());
			locCfg.set("spawns." + nextString + ".x", loc.getX());
			locCfg.set("spawns." + nextString + ".y", loc.getY());
			locCfg.set("spawns." + nextString + ".z", loc.getZ());
			locCfg.set("spawns." + nextString + ".yaw", loc.getYaw());
			locCfg.set("spawns." + nextString + ".pitch", loc.getPitch());

			saveLocCfg();

		} else {

			locCfg.set("spawns." + number + ".world", loc.getWorld().getName());
			locCfg.set("spawns." + number + ".x", loc.getX());
			locCfg.set("spawns." + number + ".y", loc.getY());
			locCfg.set("spawns." + number + ".z", loc.getZ());
			locCfg.set("spawns." + number + ".yaw", loc.getYaw());
			locCfg.set("spawns." + number + ".pitch", loc.getPitch());

			saveLocCfg();
		}

		return next;

	}

	/**
	 * Setzt einem spieler einen zufaelligen Spawnpunkt
	 * 
	 * @param p
	 *            Der Spieler, der teleportiert werden soll
	 */

	public static void randomSpawnPlayer(Player p) {

		ConfigurationSection sect = locCfg.getConfigurationSection("spawns");
		StringBuilder spawns = new StringBuilder();
		int lenght = 0;
		/*
		 * Laedt die spawnpunkte
		 */
		try {
			for (String home1 : sect.getKeys(false)) {
				if (!spawns.toString().isEmpty())
					spawns.append(", ");
				spawns.append(home1);
				lenght++;
			}
			System.out.println("lenght = " + lenght);

		} catch (NullPointerException e) {
			lenght = 0;
			System.out.println("NPE: 148");
		}
		String[] spawnsString = spawns.toString().split(", ");

		Random Random = new Random();
		int number = Random.nextInt(lenght) + 0;

		String CurrentSpawn = spawnsString[number];

		System.out.println("CurrentSppawn = " + CurrentSpawn);

		System.out.println("path = spawns." + CurrentSpawn + ".world");
		/*
		 * Laedt die Location
		 */
		World world = Bukkit.getWorld(locCfg.getString("spawns." + CurrentSpawn
				+ ".world"));
		double x = locCfg.getDouble("spawns." + CurrentSpawn + ".x");
		double y = locCfg.getDouble("spawns." + CurrentSpawn + ".y");
		double z = locCfg.getDouble("spawns." + CurrentSpawn + ".z");
		double Yaw = locCfg.getDouble("spawns." + CurrentSpawn + ".yaw");
		float Pitch = (float) locCfg.getDouble("spawns." + CurrentSpawn
				+ ".pitch");

		System.out.println("x=" + x);

		Location loc = p.getLocation();

		loc.setWorld(world);
		loc.setX(x);
		loc.setY(y);
		loc.setZ(z);
		loc.setYaw((float) Yaw);
		loc.setPitch(Pitch);

		System.out.println("Loc = " + loc);

		p.teleport(loc);

	}

	/**
	 * Speichert die Datei
	 */
	public static void saveLocCfg() {
		try {
			locCfg.save(file);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}
