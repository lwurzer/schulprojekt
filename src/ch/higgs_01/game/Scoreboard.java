package ch.higgs_01.game;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.ScoreboardManager;

public class Scoreboard {

	static ScoreboardManager boardManager = Bukkit.getScoreboardManager();
	static org.bukkit.scoreboard.Scoreboard board = boardManager
			.getNewScoreboard();
	static Objective obj;

	/**
	 * Startet das Scoreboard
	 */
	@SuppressWarnings("deprecation")
	public static void setupScoreboard() {
		/*
		 * laedt das Board, sofern es schon existiert
		 */
		if (board.getObjective("Baum") != null) {

			obj = board.getObjective("Baum");

		}
		/*
		 * Wenn Das Board noch nicht existiert wird es hier erstellt
		 */
		else {

			obj = board.registerNewObjective("Baum", "Egal");
			obj.setDisplayName("�aLeoCraft");
			obj.setDisplaySlot(DisplaySlot.SIDEBAR);

		}
		for (Player pOn : Bukkit.getOnlinePlayers()) {

			Score score = obj.getScore("" + pOn.getName());
			score.setScore(Game.getPlayerKills(pOn));

		}
		for (Player pOn : Bukkit.getOnlinePlayers()) {

			pOn.setScoreboard(board);

		}

	}

	/**
	 * Updatet das Scoreboard f�r jeden spieler auf dem Server
	 */
	@SuppressWarnings("deprecation")
	public static void updateScoreboard() {

		obj = board.getObjective(DisplaySlot.SIDEBAR);

		for (Player pOn : Bukkit.getOnlinePlayers()) {

			Score score = obj.getScore("" + pOn.getName());
			score.setScore(Game.getPlayerKills(pOn));

		}
		for (Player pOn : Bukkit.getOnlinePlayers()) {
			pOn.setScoreboard(board);
		}

	}

	/**
	 * Loescht das Scoreboard fuer jeden Spieler auf dem Server
	 */
	@SuppressWarnings("deprecation")
	public static void deleteScoreboard() {

		for (Player p : Bukkit.getOnlinePlayers()) {

			p.setScoreboard(boardManager.getNewScoreboard());

		}

	}

}
