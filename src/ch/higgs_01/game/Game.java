package ch.higgs_01.game;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.EntityEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ch.higgs_01.main.Main;
import ch.higgs_01.utils.loadConfig;

public class Game {

	public static ArrayList<String> voted = new ArrayList<String>();
	static String Prefix = loadConfig.Prefix;
	private static Main plugin;
	public static int countdownShed = 1;

	public static boolean isGame;
	public static boolean isMoveCancel;
	static int startIn;
	static int taskId_StartShed = 15;

	public static HashMap<String, Integer> Hash_kills = new HashMap<String, Integer>();

	public Game(Main main) {

		isGame = false;
		Game.plugin = main;
	}

	/**
	 * Vote to start
	 * 
	 * @param p
	 *            Player, der gevoted hat
	 */
	@SuppressWarnings({ "deprecation" })
	public static void vote(Player p) {

		if (!voted.contains(p.getName())) {

			voted.add(p.getName());

			for (Player pOnline : Bukkit.getOnlinePlayers()) {

				pOnline.sendMessage(loadConfig.Prefix
						+ "�6"
						+ p.getName()
						+ " �e hat daf�r gevotet, dass das Spiel starten sollte.");

			}
			/*
			 * Startet das Spiel, wenn genug spieler gevotet haben
			 */
			if (voted.size() >= Bukkit.getOnlinePlayers().length / 2) {
				startGame();
			}
		} else {
			p.sendMessage(Prefix + "�cDu hast schon gevoted!");
		}
	}

	/**
	 * Einem Spieler einen Kill hinzuf�gen
	 * 
	 * @param p
	 *            Der Spieler, der Get�tet hat
	 */
	public static void addPlayerKill(Player p) {
		int kills;

		if (Hash_kills.get(p.getName()) != null) {

			kills = Hash_kills.get(p.getName());

			ItemStack Arrow = new ItemStack(Material.ARROW);

			p.getInventory().addItem(Arrow);
			int killsTMP = kills + 1;
			Hash_kills.put(p.getName(), killsTMP);
			Scoreboard.updateScoreboard();
			if (killsTMP >= loadConfig.Punkte) {

				StopGame(p);

			}

		} else {

			Hash_kills.put(p.getName(), 1);

		}

	}

	/**
	 * 
	 * @param p
	 *            Der Spieler
	 * @return Die Anzahl Kills
	 */
	public static int getPlayerKills(Player p) {

		if (Hash_kills.get(p.getName()) == null) {

			return 0;

		} else {

			return Hash_kills.get(p.getName());

		}
	}

	/**
	 * Startet das Spiel
	 */
	@SuppressWarnings("deprecation")
	public static void startGame() {

		for (Player p : Bukkit.getOnlinePlayers()) {
			/*
			 * Cleart den Chatverlauf
			 */
			for (int i = 0; i <= 150; i++) {

				p.sendMessage("");

			}

		}

		for (Player p : Bukkit.getOnlinePlayers()) {
			/*
			 * Gibt dem Spieler die gegenst�nde und heilt ihn
			 */
			SpawnPoints.randomSpawnPlayer(p);
			p.setGameMode(GameMode.SURVIVAL);
			p.setLevel(0);
			p.setExp(0);
			p.setAllowFlight(false);
			p.setHealth(20D);
			p.setFoodLevel(20);
			p.getInventory().clear();

			ArrayList<ItemStack> weapons = loadConfig.items;

			for (ItemStack Item : weapons) {

				if (Item.getType() == Material.BOW) {

					Item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 100);

				}

				p.getInventory().addItem(Item);

			}

			ArrayList<ItemStack> armor = loadConfig.armor;
			ItemStack[] ArmorItems = (ItemStack[]) armor
					.toArray(new ItemStack[0]);
			p.getInventory().setArmorContents(ArmorItems);

		}
		isMoveCancel = true;
		startIn = 10;
		for (Player p : Bukkit.getOnlinePlayers()) {

			p.sendMessage(Prefix + "Das Spiel beginnt in 10 Sekunden");

		}
		/*
		 * Startet den countdown
		 */
		taskId_StartShed = Bukkit.getScheduler().scheduleSyncRepeatingTask(
				plugin, new Runnable() {

					@Override
					public void run() {

						startIn--;

						if (startIn == 0 && !isGame) {

							for (Player p : Bukkit.getOnlinePlayers()) {

								p.playSound(p.getLocation(),
										Sound.FIREWORK_BLAST, 10F, 10F);

							}

							Bukkit.broadcastMessage("�eDas Spiel hat begonnen - Wer zuerst �6"
									+ loadConfig.Punkte
									+ " �eKills erreicht, hat gewonnen!");
							Bukkit.broadcastMessage("                       �e�l---�6�lViel Gl�ck�e�l---");
							for (Player p : Bukkit.getOnlinePlayers()) {

								Hash_kills.put(p.getName(), 0);

							}
							Scoreboard.setupScoreboard();

							isMoveCancel = false;
							isGame = true;
							for (Player p : Bukkit.getOnlinePlayers()) {

								p.setCanPickupItems(false);

							}

							Bukkit.getScheduler().cancelTask(taskId_StartShed);
							return;

						} else if (isGame) {
							isGame = true;
							isMoveCancel = false;
							Bukkit.getScheduler().cancelTask(taskId_StartShed);
							return;

						} else {

							if (startIn <= 0) {
								isGame = true;
								isMoveCancel = false;
								Bukkit.getScheduler().cancelTask(
										taskId_StartShed);
								return;

							}

							Bukkit.broadcastMessage(Prefix + startIn);
							for (Player p : Bukkit.getOnlinePlayers()) {

								p.playSound(p.getLocation(), Sound.NOTE_PLING,
										10F, 10F);

							}

						}

					}
				}, 20L, 20L);

	}

	/**
	 * Beendet das Spiel
	 * 
	 * @param winner
	 *            Der Spieler, der Gewonnen hat
	 */
	@SuppressWarnings("deprecation")
	public static void StopGame(Player winner) {

		Location loc = SpawnPoints.getLobbyLoc();
		for (Player p : Bukkit.getOnlinePlayers()) {

			p.getInventory().clear();
			p.teleport(loc);
			winner.playEffect(EntityEffect.FIREWORK_EXPLODE);
			p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL, 5, 5);

		}

		Bukkit.broadcastMessage("�e�l----------�6�lENDE�e�l----------");
		Bukkit.broadcastMessage("�6�l" + winner.getName()
				+ " �ehat �6�lgewonnen�e!");
		Bukkit.broadcastMessage("�e�l----------�6�lENDE�e�l----------");

		Scoreboard.deleteScoreboard();

		voted.clear();

		isGame = false;

	}

}
