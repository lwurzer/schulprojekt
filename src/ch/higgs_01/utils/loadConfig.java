package ch.higgs_01.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import ch.higgs_01.main.Main;

public class loadConfig {
	/**
	 * Definiere die Variabeln der Konfigurationsdatei
	 */

	public static String Prefix;
	public static int maxPlayers;
	public static int minPlayers;
	public static int Punkte;
	public static ArrayList<ItemStack> items = new ArrayList<ItemStack>();
	public static ArrayList<ItemStack> armor = new ArrayList<ItemStack>();
	public static int CountdownTime;

	public static String lang_ERROR;
	public static String lang_NO_PERMISSION;
	public static String lang_PLAYER_NOT_EXISTS;

	static String pfad = "plugins/LeoCraft";
	static File file = new File(pfad, "config.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	static File langFile = new File(pfad, "lang.yml");
	static FileConfiguration langCfg = YamlConfiguration
			.loadConfiguration(langFile);

	/**
	 * �berpr�ft, ob das verzeichnis existiert
	 */
	static void checkOrdner() {

		File check2 = new File(pfad);
		if (!check2.isDirectory()) {
			check2.mkdirs();
		}

		File check = new File(pfad);
		if (!check.isDirectory()) {
			check.mkdirs();
		}

	}

	/**
	 * Erstellt die Konfigurationsdatei
	 */
	public static void createConfig() {

		ArrayList<String> armorID = new ArrayList<String>();
		armorID.add(298 + ":0:1");
		armorID.add(299 + ":0:1");
		armorID.add(300 + ":0:1");
		armorID.add(301 + ":0:1");

		ArrayList<String> waffenID = new ArrayList<String>();
		waffenID.add(261 + ":0:1");
		waffenID.add(272 + ":0:1");

		if (!file.exists()) {

			cfg.addDefault("Allgemein.Prefix", "�6[�cLC�6] �c");
			cfg.addDefault("Spiel.maxSpieler", 4);
			cfg.addDefault("Spiel.minSpieler", 2);
			cfg.addDefault("Spiel.Punkte", 10);
			cfg.addDefault("Items.Ruestung", armorID);
			cfg.addDefault("Items.Waffen", waffenID);
			cfg.addDefault("Spiel.CountdownTime", 30);
			cfg.options().copyDefaults(true);
			try {
				cfg.save(file);
				Main.logInfo("Konfigurationsdatei wurde erstellt!");
			} catch (IOException e) {

			}

		}
		if (!langFile.exists()) {

			langCfg.addDefault("NO_PERMISSION",
					"&cFehler: �4Du hast zu wenig Rechte!");
			langCfg.addDefault("ERROR", "&cFehler: &4%error%");
			langCfg.addDefault("PLAYER_NOT_EXISTS",
					"&cFehler: &4Dieser Spieler existiert nicht!");

			langCfg.options().copyDefaults(true);
			try {
				langCfg.save(langFile);
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

	}

	/**
	 * L�dt die Konfiguratiosndatei in die Variabeln
	 */
	public static void loadPluginConfig() {

		Prefix = cfg.getString("Allgemein.Prefix");
		maxPlayers = cfg.getInt("Spiel.maxSpieler");
		minPlayers = cfg.getInt("Spiel.minSpieler");
		Punkte = cfg.getInt("Spiel.Punkte");
		CountdownTime = cfg.getInt("Spiel.CountdownTime");
		for (String item : cfg.getStringList("Items.Ruestung")) {

			String parts[] = item.split(":");
			int id = Integer.parseInt(parts[0]);
			short shortID = Short.parseShort(parts[1]);
			int amount = Integer.parseInt(parts[2]);

			armor.add(new ItemStack(IDAPI.getMaterialById(id), amount, shortID));

		}
		for (String item : cfg.getStringList("Items.Waffen")) {

			String parts[] = item.split(":");
			int id = Integer.parseInt(parts[0]);
			short shortID = Short.parseShort(parts[1]);
			int amount = Integer.parseInt(parts[2]);

			items.add(new ItemStack(IDAPI.getMaterialById(id), amount, shortID));

		}
		lang_NO_PERMISSION = Prefix
				+ langCfg.getString("NO_PERMISSION").replace("&", "�");
		lang_ERROR = Prefix + langCfg.getString("ERROR").replace("&", "�");
		lang_PLAYER_NOT_EXISTS = Prefix
				+ langCfg.getString("PLAYER_NOT_EXISTS").replace("&", "�");

	}

}
