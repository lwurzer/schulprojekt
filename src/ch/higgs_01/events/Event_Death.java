package ch.higgs_01.events;

import java.util.ArrayList;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

import ch.higgs_01.game.Game;
import ch.higgs_01.game.SpawnPoints;
import ch.higgs_01.utils.loadConfig;

public class Event_Death implements Listener {
	/**
	 * Wenn ein Spieler Schaden bekommt
	 * 
	 * @param e
	 *            Der Event, der ausgefuehrt wird, wenn der Spieler schaden
	 *            bekommt
	 */
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerDamage(EntityDamageByEntityEvent e) {

		if (!Game.isGame) {
			e.setCancelled(true);
			return;
		}

		if (e.getDamager() == e.getEntity()) {
			e.setCancelled(true);
			return;
		}
		if (e.getDamager().getType() == EntityType.ARROW
				&& ((Arrow) e.getDamager()).getShooter() == e.getEntity()) {
			e.setCancelled(true);
			return;
		}

		Entity damaged = e.getEntity();
		Entity damager = e.getDamager();
		if(e.getCause() == DamageCause.FALL){
			e.setCancelled(true);
		}

		if (!(damaged instanceof Player) && !(damager instanceof Player)
				&& !(damager.getType() == EntityType.ARROW)) {
			System.out.println(damager.getType());
			return;

		}

		Player damagedPlayer;
		Player damagerPlayer;

		if (damager.getType() == EntityType.ARROW) {

			damagedPlayer = (Player) damaged;

			Arrow arrow = ((Arrow) e.getDamager());
			damagerPlayer = (Player) arrow.getShooter();
			e.setDamage(50D);

		} else {
			damagedPlayer = (Player) damaged;
			damagerPlayer = (Player) damager;

		}
		/*
		 * Wenn man von einem Pfeil getrofen wird.
		 */
		if (e.getCause() == DamageCause.PROJECTILE) {

			e.setDamage(50.0);

		}
		/*
		 * Ueberpruefe, ob der Spieler sterben wuerde
		 */
		if ((((CraftPlayer) damagedPlayer).getHealth() - e.getDamage()) <= 0) {

			e.setCancelled(true);

			damagedPlayer.sendMessage(loadConfig.Prefix + "�aDu wurdest von �2"
					+ damagerPlayer.getName() + "�a get�tet");
			damagerPlayer.sendMessage(loadConfig.Prefix + "�aDu hast �2"
					+ damagedPlayer.getName() + "�a get�tet");
			SpawnPoints.randomSpawnPlayer(damagedPlayer);

			damagedPlayer.getInventory().clear();

			damagedPlayer.setGameMode(GameMode.SURVIVAL);
			damagedPlayer.setLevel(0);
			damagedPlayer.setExp(0);
			damagedPlayer.setAllowFlight(false);
			damagedPlayer.setHealth(20D);
			damagedPlayer.setFoodLevel(20);

			ArrayList<ItemStack> weapons = loadConfig.items;

			for (ItemStack Item : weapons) {

				if (Item.getType() == Material.BOW) {

					Item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 100);
				}

				damagedPlayer.getInventory().addItem(Item);

			}

			ArrayList<ItemStack> armor = loadConfig.armor;
			ItemStack[] ArmorItems = armor.toArray(new ItemStack[0]);
			damagedPlayer.getInventory().setArmorContents(ArmorItems);

			Game.addPlayerKill(damagerPlayer);

		}

	}

	/**
	 * Wenn ein Spieler von einem Block schaden bekommt
	 * 
	 * @param e
	 *            Der Event
	 */
	@EventHandler
	public void onPlayerDamageByBlock(EntityDamageByBlockEvent e) {

		e.setCancelled(true);

	}

}
