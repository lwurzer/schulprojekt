package ch.higgs_01.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import ch.higgs_01.game.Game;

public class Event_ItemDrop implements Listener {

	/**
	 * Wenn ein Spieler einen gegenstand fallen laesst
	 * 
	 * @param e
	 */
	@EventHandler
	public void onPlayerItemDrop(PlayerDropItemEvent e) {

		e.setCancelled(true);

	}

	/**
	 * Wenn ein Spieler im Inventar die Items verschieben will
	 * 
	 * @param e
	 */
	@EventHandler
	public void onPlayerInventoryInteract(InventoryInteractEvent e) {

		if (Game.isGame) {

			e.setCancelled(true);

		}

	}

}
