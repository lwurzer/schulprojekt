package ch.higgs_01.events;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

public class Event_ProjectileHit implements Listener {

	/**
	 * Entfernt alle Arrows, die auf dem Boden auftreffen
	 * 
	 * @param e
	 */
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent e) {

		if (e.getEntityType() == EntityType.ARROW) {
			/*
			 * Entfernt den Pfeil
			 */
			e.getEntity().remove();
			

		}

	}

}
