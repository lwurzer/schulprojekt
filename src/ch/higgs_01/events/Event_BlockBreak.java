package ch.higgs_01.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import ch.higgs_01.game.Game;

public class Event_BlockBreak implements Listener {
	/**
	 * Wenn ein block zerstoert wird
	 * 
	 * @param e
	 *            Der Event
	 */
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {

		if (Game.isGame == false && e.getPlayer().isOp()) {

		} else {
			e.setCancelled(true);
		}

	}

	/**
	 * Wenn ein block platziert wird
	 * 
	 * @param e
	 *            Der Event
	 */
	@EventHandler
	public void onblockPlace(BlockPlaceEvent e) {

		if (Game.isGame == false && e.getPlayer().isOp()) {

		} else {
			e.setCancelled(true);
		}

	}

}
