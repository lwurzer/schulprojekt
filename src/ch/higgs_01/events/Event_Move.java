package ch.higgs_01.events;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import ch.higgs_01.game.Game;

public class Event_Move implements Listener {
	/**
	 * Wenn sich ein spieler bewegt
	 * 
	 * @param e
	 *            Der Event
	 */
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		/*
		 * teleportiert den Spieler wieder zurueck
		 */
		if (Game.isMoveCancel) {
			Location loc = e.getPlayer().getLocation();
			loc.setX(e.getFrom().getX());
			loc.setY(e.getFrom().getY());
			loc.setZ(e.getFrom().getZ());
			loc.setYaw(e.getTo().getYaw());
			loc.setPitch(e.getTo().getPitch());
			loc.setWorld(e.getFrom().getWorld());
			e.getPlayer().teleport(loc);

		}

	}

}
