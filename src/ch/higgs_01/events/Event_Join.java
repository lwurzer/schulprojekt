package ch.higgs_01.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import ch.higgs_01.game.Game;
import ch.higgs_01.main.Main;
import ch.higgs_01.utils.loadConfig;

public class Event_Join implements Listener {

	private Main main;

	public Event_Join(Main main) {

		this.main = main;

	}

	static boolean isEnoughPlayers = false;
	static int time;
	int taskid = 255;

	/**
	 * Wenn ein Spieler den Server betritt
	 * 
	 * @param e
	 */
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent e) {
		/*
		 * Ueberprueft ob das Spiel schon gestartet wurde
		 */
		if (Game.isGame) {
			
			e.disallow(
					PlayerLoginEvent.Result.KICK_OTHER,
					"�4�lFehler �r\n�6�lDas Spiel hat bereits gestartet. Probiere es sp�ter Nochmals!");

		}

		if (Bukkit.getOnlinePlayers().length + 1 > loadConfig.maxPlayers) {

			e.disallow(PlayerLoginEvent.Result.KICK_OTHER,
					"�4�lFehler �r\nDas Spiel ist voll! Versuche es sp�ter nochmals!");

		}

	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {

		e.setJoinMessage("�8[�7+�8] �7" + e.getPlayer().getName());

		if (Bukkit.getOnlinePlayers().length >= loadConfig.minPlayers) {

			if (Bukkit.getScheduler().isCurrentlyRunning(taskid)) {
				return;
			}
			isEnoughPlayers = true;
			time = loadConfig.CountdownTime;
			/*
			 * Scheduler: Wenn genuegend Spieler auf dem Server sind startet das Spiel
			 */
			taskid = Bukkit.getScheduler().scheduleSyncRepeatingTask(main,
					new Runnable() {

						@Override
						public void run() {

							if (Game.isGame) {

								Bukkit.getScheduler().cancelTask(taskid);

							}

							if (!(Bukkit.getOnlinePlayers().length >= loadConfig.minPlayers)
									&& isEnoughPlayers) {
								isEnoughPlayers = false;
								Bukkit.broadcastMessage(loadConfig.Prefix
										+ "Countdown angehalten");

								time = loadConfig.CountdownTime;
								Bukkit.getScheduler().cancelTask(taskid);

							} else if (!isEnoughPlayers) {

							} else {
								isEnoughPlayers = true;
								for (Player p : Bukkit.getOnlinePlayers()) {
									p.setLevel(0);
									p.setExp(0);
									p.setLevel(time);
									p.setExp(0.07F);

								}

								if (time == 30) {
									Bukkit.broadcastMessage(loadConfig.Prefix
											+ "Das Spiel startet in 30 Sekunden");
								}
								if (time == 20) {
									Bukkit.broadcastMessage(loadConfig.Prefix
											+ "Das Spiel startet in 20 Sekunden");
								}
								if (time == 15) {
									Bukkit.broadcastMessage(loadConfig.Prefix
											+ "Das Spiel startet in 15 Sekunden");
								}
								if (time == 10) {

									Game.startGame();
									Bukkit.getScheduler().cancelTask(taskid);

								}
								time--;

							}
						}
					}, 0L, 20L);

		}
	}

}
