package ch.higgs_01.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import ch.higgs_01.game.Game;
import ch.higgs_01.game.Scoreboard;

public class Event_Leave implements Listener {
	/**
	 * Wenn ein spieler den Server verlaesst
	 * 
	 * @param e
	 */
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {

		if (Game.Hash_kills.containsKey(e.getPlayer().getName())) {

			Game.Hash_kills.remove(e.getPlayer().getName());

		}
		e.setQuitMessage("�8[�7-�8] �7" + e.getPlayer().getName());
		Scoreboard.updateScoreboard();

	}

	/**
	 * Wenn ein Spieler gekickt wird
	 * 
	 * @param e
	 */
	@EventHandler
	public void onPlayerGetKicked(PlayerKickEvent e) {

		if (Game.Hash_kills.containsKey(e.getPlayer().getName())) {

			Game.Hash_kills.remove(e.getPlayer().getName());

		}
		e.setLeaveMessage("�8[�7+�8] �7" + e.getPlayer().getName()
				+ " - Er wurde wegen '" + e.getReason() + "' gekickt");
		Scoreboard.updateScoreboard();
	}

}
