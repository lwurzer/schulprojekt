package ch.higgs_01.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ch.higgs_01.game.Game;
import ch.higgs_01.utils.loadConfig;

public class CMD_Game implements CommandExecutor{

	
	loadConfig Config = new loadConfig();
	/**
	 * Befehle, die Das Spiel betreffen
	 * @param cs Derjenige, der den Befehl gesendet hat.
	 * @param cmd Der Befehl
	 * @param args Argumente
	 */
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if(!(cs instanceof Player)){
			cs.sendMessage(loadConfig.Prefix + "�4Fehler: �cDu kannst das nicht tun!");
			return true;
		}
		Player p = (Player) cs;
		if(cmd.getName().equalsIgnoreCase("lc")){
			
			if(args.length == 0){
			/*
			 * Hilfe	
			 */
			p.sendMessage("�8-----------�eLeoCraft�8----------");
			
			p.sendMessage(" �6/lc leave");
			p.sendMessage(" �6/lc vote");
			p.sendMessage(" �6/lc start");
			p.sendMessage(" �6/lc stop");
			p.sendMessage(" �6/lcadmin");
			p.sendMessage("�8-----------�eLeoCraft�8----------");
			return true;
				
			}
			if(args.length == 1){
				
				
				
				if(args[0].equalsIgnoreCase("vote")){
					
					Game.vote(p);
					
					
				}
				if(args[0].equalsIgnoreCase("start")){
					
					if(Game.isGame){
						
						p.sendMessage(loadConfig.Prefix + "�4Fehler: �cDas Spiel hat bereits begonnen. Nutze �4/lc stop�c um das Spiel zu beenden.");
						return true;
						
					}
					
					if(!p.hasPermission("lc.forcestart")){
						p.sendMessage(loadConfig.Prefix + "�4Fehler: �cDu hast zu wenig Rechte!");
						return true;
					}
					Game.startGame();
					return true;
					
				}
				if(args[0].equalsIgnoreCase("stop")){
					if(!cs.hasPermission("lc.stop")){
						cs.sendMessage(loadConfig.lang_NO_PERMISSION);
						return true;
					}
					
					if(!Game.isGame){
						
						p.sendMessage(loadConfig.Prefix + "�4Fehler: �cDas Spiel hat noch nicht begonnen. Nutze �4/lc start�c um das Spiel zu starten.");
						return true;
						
					}
					
					Game.StopGame(Bukkit.getOnlinePlayers()[0]);
					Bukkit.broadcastMessage("�eDas Spiel wurde von �6" + cs.getName() + "�e gestoppt!");
					return true;
					
				}
			}
		}
		
		return true;
	}
	
}
