package ch.higgs_01.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.command.ConsoleCommandCompleter;
import org.bukkit.entity.Player;

import ch.higgs_01.game.SpawnPoints;
import ch.higgs_01.utils.loadConfig;

public class CMD_Admin implements CommandExecutor {

	/**
	 * Befehle, die Admins betreffen
	 * 
	 * @param cs
	 *            Derjenige, der den Befehl gesendet hat.
	 * @param cmd
	 *            Der Befehl
	 * @param args
	 *            Argumente
	 */
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("lcadmin")) {

			if (cs instanceof ConsoleCommandCompleter) {

				cs.sendMessage(loadConfig.lang_ERROR.replace("%error%",
						"Die Konsole kann das nicht tun!"));
				return true;

			}
			Player p = (Player) cs;
			if (!p.hasPermission("lc.admin")) {
				p.sendMessage(loadConfig.lang_NO_PERMISSION);
				return true;
			}

			if (args.length == 0) {
				/*
				 * Hilfe zu allen Befehlen
				 */
				p.sendMessage("�8----------�eLC-Admin�8----------");
				p.sendMessage(" �6/lcadmin setspawn [number]");
				p.sendMessage(" �6/lcadmin setlobby");
				p.sendMessage("   �6aliases: �e"
						+ Bukkit.getPluginCommand("lcadmin").getAliases()
								.toString());
				p.sendMessage("�8----------�eLC-Admin�8----------");
				return true;
			}
			if (args[0].equalsIgnoreCase("setspawn")) {

				String Number;

				if (args.length == 2) {

					Number = args[1];

				} else {
					Number = "next";
				}
				int next = SpawnPoints.setSpawnPoint(p, Number);
				p.sendMessage(loadConfig.Prefix + "�aSpawnpunkt �2#" + next);
				return true;

			}
			if (args[0].equalsIgnoreCase("setlobby")) {

				SpawnPoints.setLobbyPoint(p);
				p.sendMessage(loadConfig.Prefix
						+ "Der Spawnpunkt wurde erfolgreich gesetzt.");
				return true;

			}

		}

		return true;
	}

}
